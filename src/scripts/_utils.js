const utils = {
  cache: {},
  // сет брейкпоинтов для js
  // должны совпадать с теми что в body:after
  mediaBreakpoint: {
    sm: 576,
    md: 768,
    lg: 992,
    xl: 1366,
    xxl: 1920,
  },
  isMobile: {
    Android() {
      return navigator.userAgent.match(/Android/i);
    },
    BlackBerry() {
      return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS() {
      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera() {
      return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows() {
      return navigator.userAgent.match(/IEMobile/i);
    },
    any() {
      return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
    },
  },
  svgIcons() {
    const container = document.querySelector('[data-svg-path]');
    const path = container.getAttribute('data-svg-path');
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      container.innerHTML = xhr.responseText;
    };
    xhr.open('get', path, true);
    xhr.send();
  },
  detectIE() {
    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */

    (function detectIE() {
       var ua = window.navigator.userAgent;

       var msie = ua.indexOf('MSIE ');
       if (msie > 0) {
        // IE 10 or older => return version number
        var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        document.querySelector('body').className += ' IE';
      }

      var trident = ua.indexOf('Trident/');
      if (trident > 0) {
        // IE 11 => return version number
        var rv = ua.indexOf('rv:');
        var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        document.querySelector('body').className += ' IE';
      }

      var edge = ua.indexOf('Edge/');
      if (edge > 0) {
        // IE 12 (aka Edge) => return version number
        var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        document.querySelector('body').className += ' IE';
      }

      // other browser
      return false;
    })();
  },
  truncateText(elements) {
    const cutText = () => {
      for (let i = 0; i < elements.length; i++) {
        const text = elements[i];
        const elemMaxHeight = parseInt(getComputedStyle(text).maxHeight, 10);
        const elemHeight = text.offsetHeight;
        const maxHeight = elemMaxHeight ? elemMaxHeight : elemHeight;

        shave(text, maxHeight);
      }
    };

    this.cache.cutTextListener = this.throttle(cutText, 100);

    cutText();

    window.addEventListener('resize', this.cache.cutTextListener);
  },
  throttle(callback, limit) {
    let wait = false;

    return () => {
      if (!wait) {
        callback.call();
        wait = true;
        setTimeout(() => {
          wait = false;
        }, limit);
      }
    };
  },
  getScreenSize() {
    let screenSize = window.getComputedStyle(document.querySelector('body'), ':after').getPropertyValue('content');
    screenSize = parseInt(screenSize.match(/\d+/), 10);
    return screenSize;
  },
  polyfills() {
    /**
     * polyfill for elem.closest
     */
    (function(ELEMENT) {
      ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;
      ELEMENT.closest = ELEMENT.closest || function closest(selector) {
        if (!this) return null;
        if (this.matches(selector)) return this;
        if (!this.parentElement) {
          return null;
        } else {
          return this.parentElement.closest(selector);
        } 
      };
    }(Element.prototype));

    /**
     * polyfill for elem.hasClass
     */
    Element.prototype.hasClass = function(className) {
      return this.className && new RegExp("(^|\\s)" + className + "(\\s|$)").test(this.className);
    };
  },
};

export default utils;