import Mask from './_mask';
import utils from './_utils';

const APP = {
  initBefore() {
    utils.polyfills();
    utils.svgIcons();
    document.documentElement.className = document.documentElement.className.replace('no-js', 'js');
  },

  init() {
    utils.detectIE();
    APP.lazyload();

    const myMask = new Mask('.js-tel');
    myMask.init();

    APP.buttons();
    APP.closeOnFocusLost();
  },

  initOnLoad() {
    utils.truncateText(document.querySelectorAll('.js-dot'));
  },

  buttons() {
    Array.prototype.forEach.call(
      document.querySelectorAll('.menu-trigger'), (item) => {
        item.addEventListener('click', () => {
          document.body.classList.toggle('nav-showed');
        });
      },
    );
  },

  closeOnFocusLost() {
    document.addEventListener('click', (e) => {
      const trg = e.target;
      if (!trg.closest('.header')) {
        document.body.classList.remove('nav-showed');
      }
    });
  },

  lazyload() {
    function update() {
      APP.myLazyLoad.update();
      APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
    }

    function regularInit() {
      APP.myLazyLoad = new LazyLoad({
        elements_selector: '.lazyload',
        callback_error(el) {
          el.parentElement.classList.add('lazyload-error');
        },
      });
      APP.objectFitFallback(document.querySelectorAll('[data-object-fit]'));
    }

    if (typeof APP.myLazyLoad === 'undefined') {
      regularInit();
    } else {
      update();
    }
  },

  objectFitFallback(selector) {
    // if (true) {
    if ('objectFit' in document.documentElement.style === false) {
      for (let i = 0; i < selector.length; i += 1) {
        const that = selector[i];
        const imgUrl = that.getAttribute('src') ? that.getAttribute('src') : that.getAttribute('data-src');
        const dataFit = that.getAttribute('data-object-fit');
        let fitStyle;
        if (dataFit === 'cover') {
          fitStyle = 'cover';
        } else {
          fitStyle = 'contain';
        }
        const parent = that.parentElement;
        if (imgUrl) {
          parent.style.backgroundImage = `url(${imgUrl})`;
          parent.classList.add('fit-img');
          parent.classList.add(`fit-img--${fitStyle}`);
        }
      }
    }
  },
};

APP.initBefore();

document.addEventListener('DOMContentLoaded', () => {
  APP.init();
});

window.onload = () => {
  APP.initOnLoad();
};
